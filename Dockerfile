FROM node:lts AS builder

WORKDIR /srv

RUN npm i -g pnpm

COPY ./webapp-gestocks .

RUN pnpm i
RUN pnpm run build

FROM nginx:latest AS prod

COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /srv/dist /usr/share/nginx/html
